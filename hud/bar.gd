extends Sprite

var value setget _set_value
var cur_anim = "ok"
func _set_value( v ):
	if v > 1 or v < 0: return
	value = v
	var r = $value.region_rect
	r.size.x = ( v * 30 )
	$value.region_rect = r
	if value > 0.2:
		if cur_anim != "ok":
			cur_anim = "ok"
			$AnimationPlayer.play( "ok" )
	else:
		if cur_anim != "small":
			cur_anim = "small"
			$AnimationPlayer.play( "small" )