extends "res://player/states/state.gd"

var movetime
var lst_change_dir

func initialize( obj ):
	obj.anim_nxt = "walk"
	movetime = rand_range( 2, 5 )
	lst_change_dir = 0

func run_state( obj, delta ):
	movetime -= delta
	if movetime <= 0:
		obj.state_nxt = obj.STATES.IDLE
	
	# gravity
	obj.vel.y = min( obj.vel.y + obj.GRAVITY * delta, obj.TER_VEL )
	
	# dampen horizontal motion
	obj.vel.x = obj.rotate.scale.x * obj.WALK_VEL
	
	# move
	obj.vel = obj.move_and_slide( obj.vel, Vector2( 0, -1 ) )
	
#	if obj.is_obstacle():
#		obj.dir_nxt = -obj.rotate.scale.x
	lst_change_dir += delta
	if obj.is_obstacle():
		if lst_change_dir > 0.15:
			obj.dir_nxt = -obj.rotate.scale.x
		else:
			obj.state_nxt = obj.STATES.IDLE
		
	if obj.player_in_sight( true, 1 ):
		obj.state_nxt = obj.STATES.HUNT