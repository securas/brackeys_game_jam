extends "res://player/states/state.gd"

enum STATES { FLY, GROUND, REVIVE, FINISH }
var state = -1
var finished_flying = false
var return_timer

func initialize( obj ):
	state = STATES.FLY
	finished_flying = false
	obj.anim_nxt = "hit_front"
	obj.dir_nxt = -obj.hit_dir
	obj.vel.x = obj.hit_dir * obj.HIT_THROWBACK
	return_timer = obj.REVIVE_DURATION
	obj.hitbox.set_collision_layer_bit( 12, false )
	game.play_sfx( game.SFX_ZOMBIE_ROAR )

func run_state( obj, delta ):
	
	# gravity
	obj.vel.y = min( obj.vel.y + obj.GRAVITY * delta, obj.TER_VEL )
	
	# dampen horizontal motion
	#obj.vel.x = 0#lerp( obj.vel.x, 0, delta * obj.DECEL )
	
	# move
	
	obj.vel = obj.move_and_slide( obj.vel, Vector2( 0, -1 ) )
	obj.vel.x *= 0.96
	
	if state == STATES.FLY:
		# do nothing until finished flying
		if finished_flying and obj.is_on_floor():
			state = STATES.GROUND
	elif state == STATES.GROUND:
		obj.anim_nxt = "hit_front_ground"
		return_timer -= delta
		if return_timer <= 0:
			state = STATES.REVIVE
	elif state == STATES.REVIVE:
		obj.anim_nxt = "hit_front_revive"
	elif state == STATES.FINISH:
		obj.is_hit = false
		obj.state_nxt = obj.STATES.IDLE
		obj.hitbox.set_collision_layer_bit( 12, true )

func finished_fly():
	finished_flying = true

func finished_revive():
	state = STATES.FINISH
