extends "res://player/states/state.gd"

var giveup_timer = -1
var lst_change_dir

func initialize( obj ):
	obj.anim_nxt = "hunt"
	giveup_timer = 2
	lst_change_dir = 0

func run_state( obj, delta ):
	#if start_attack:
		
	if not obj.attack_area.get_overlapping_areas().empty():
		obj.state_nxt = obj.STATES.ATTACK
	# gravity
	obj.vel.y = min( obj.vel.y + obj.GRAVITY * delta, obj.TER_VEL )
	
	# dampen horizontal motion
	obj.vel.x = obj.rotate.scale.x * obj.RUN_VEL
	
	# move
	obj.vel = obj.move_and_slide( obj.vel, Vector2( 0, -1 ) )
	
	lst_change_dir += delta
	if obj.is_obstacle():
		if lst_change_dir > 0.15:
			obj.dir_nxt = -obj.rotate.scale.x
		else:
			obj.state_nxt = obj.STATES.IDLE
	
	if obj.player_in_sight():
		giveup_timer = 2
		if obj.is_obstacle():
			obj.dir_nxt = -obj.rotate.scale.x
			obj.state_nxt = obj.STATES.IDLE
		else:
			var dist = game.player.global_position - obj.global_position
			if sign( dist.x ) > 0 :
				obj.dir_nxt = 1
			elif sign( dist.x ) < 0:
				obj.dir_nxt = -1
	else:
		giveup_timer -= delta
		if giveup_timer <= 0:
			obj.state_nxt = obj.STATES.IDLE


