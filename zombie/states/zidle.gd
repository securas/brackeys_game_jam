extends "res://player/states/state.gd"

var viewtimer = 0.3
var idletime
var has_hunted = false

func initialize( obj ):
	obj.anim_nxt = "idle"
	viewtimer = 0.3
	idletime = rand_range( 1, 5 )
	if not obj.always_idle:
		has_hunted = true

func run_state( obj, delta ):
	if has_hunted:
		idletime-= delta
		if idletime <= 0:
			obj.state_nxt = obj.STATES.MOVE
	# gravity
	obj.vel.y = min( obj.vel.y + obj.GRAVITY * delta, obj.TER_VEL )
	
	# dampen horizontal motion
	obj.vel.x = 0#lerp( obj.vel.x, 0, delta * obj.DECEL )
	
	# move
	obj.vel = obj.move_and_slide( obj.vel, Vector2( 0, -1 ) )
	
	# player
	var viewdir = 1
	if obj.get_node( "anim" ).is_playing() and obj.get_node( "anim" ).current_animation_position > 4:
		viewdir = -1
	if obj.player_in_sight( true, viewdir ):
		if viewdir == 1:
			viewtimer = 0.3
			obj.state_nxt = obj.STATES.HUNT
			has_hunted = true
		else:
			viewtimer -= delta
			if viewtimer <= 0:
				obj.state_nxt = obj.STATES.HUNT
				has_hunted = true
				obj.dir_nxt = -obj.rotate.scale.x
	


