extends "res://player/states/state.gd"


func initialize( obj ):
	obj.anim_nxt = "attack"
	game.player.hit( obj.scale.x )
	game.play_sfx( game.SFX_ZOMBIE_ROAR )


func run_state( obj, delta ):
	# gravity
	obj.vel.y = min( obj.vel.y + obj.GRAVITY * delta, obj.TER_VEL )
	
	# dampen horizontal motion
	obj.vel.x = 0#lerp( obj.vel.x, 0, delta * obj.DECEL )
	
	# move
	obj.vel = obj.move_and_slide( obj.vel, Vector2( 0, -1 ) )
	
	


