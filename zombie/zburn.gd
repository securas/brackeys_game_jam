extends "res://player/states/state.gd"

enum STATES { GROUND, FINISH }
var state = -1
var timer

func initialize( obj ):
	state = STATES.GROUND
	timer = 5
	obj.anim_nxt = "burn"
	#obj.dir_nxt = -obj.hit_dir
	#obj.vel.x = obj.hit_dir * obj.HIT_THROWBACK
	#obj.hitbox.set_collision_layer_bit( 12, false )
	

func run_state( obj, delta ):
	#return
	# gravity
	obj.vel.y = min( obj.vel.y + obj.GRAVITY * delta, obj.TER_VEL )
	
	# dampen horizontal motion
	#obj.vel.x = 0#lerp( obj.vel.x, 0, delta * obj.DECEL )
	
	# move
	
	obj.vel = obj.move_and_slide( obj.vel, Vector2( 0, -1 ) )
	obj.vel.x *= 0.96
	
	if state == STATES.GROUND:
		timer -= delta
		if timer <= 0:
			state = STATES.FINISH
	elif state == STATES.FINISH:
		print( "deaad zombie" )
		obj.queue_free()

