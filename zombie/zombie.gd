extends KinematicBody2D


# constants
const WALK_VEL = 15
const RUN_VEL = 50
#const ACCEL = 10 # horizontal acceleration
#const DECEL = 100 # horizontal deceleration
const MAX_VEL_AIR = 30 # max velocity on air
const ACCEL_AIR = 5 # horizontal acceleration on air
const DECEL_AIR = 5 # horizontal decceleration on air
const GRAVITY = 1000 
const TER_VEL = 150 # terminal velocity
#const JUMP_VEL = 200 # jump velocity
#const JUMP_GRAV = 500 # gravity during initial part of the jump
#const JUMP_TIME = 0.4 # time during jump to restore normal gravity
#const JUMP_MARGIN = 0.15 # margin when falling when it is still possible to jump
#const LARGE_FALL = 1 # if a fall lasts this long, there will be some recovery time
#const LAND_TIME = 0.5 # time to wait if the fall is very large

const REVIVE_DURATION = 3
const HIT_THROWBACK = 30

export var always_idle = false
export var long_ray = false
export var short_ray = false

# states machine
onready var STATES = { \
	"IDLE": $states/zidle, \
	"MOVE": $states/zmove, \
	"HUNT": $states/zhunt, \
	"HIT": $states/zhit, \
	"ATTACK": $states/zattack, \
	"BURN": $states/zburn, \
#	"WALL_JUMP": $states/wall_jump, \
#	"DASH": $states/dash \
	}
var state_cur = null
onready var state_nxt = STATES.IDLE setget _set_state_nxt



# motion
var vel = Vector2()

# direction
export( int ) var dir_nxt = 1
onready var rotate = $rotate
#onready var down = $down
onready var hitbox = $hitbox
onready var attack_area = $rotate/attack

# animation
var anim_cur =""
var anim_nxt = "idle"
var idle_animation = "idle"

# damage
var is_hit = false
var is_dead = false
var hit_dir = 0

func _set_state_nxt( v ):
	# before setting the next state, check if hit
	# prevents other states from changing the next state
	if not is_hit:
		state_nxt = v

func _ready():
	pass
	
var ctimer = 0.5 # some error with initializaqtion
func _physics_process( delta ):
	# update state
	if state_nxt != state_cur:
		if state_cur != null:
			if name == "zombie11":
				print( name, " changing from state ", state_cur.name, " to ", state_nxt.name )
			state_cur.terminate( self )
		state_cur = state_nxt
		state_cur.initialize( self )
	
	# run state
	state_cur.run_state( self, delta )
	
	# update direction
	if dir_nxt != rotate.scale.x:
		rotate.scale.x = dir_nxt
	
	# update animation
	if anim_nxt != anim_cur:
		anim_cur = anim_nxt
		$anim.play( anim_cur )
		if anim_cur == "idle":
			var p = rand_range( 0, 2 )
			$anim.seek( p )
	
	# check shadow
#	if name == "xxx":
#		print( $check_shadow.is_colliding(), " ", $check_shadow.get_collider() )
#		if $check_shadow.get_collider() != null:
#			print( $check_shadow.get_collider().name )
	if ctimer > 0:
		ctimer -= delta
	else:
		if not $check_shadow.is_colliding() and not is_hit:
			burn( rotate.scale.x )

func hit( dir, show_blood = true ):
	if is_hit: return
	state_nxt = STATES.HIT
	is_hit = true
	hit_dir = dir
	if show_blood:
		var b = preload( "res://zombie/fx/blood.tscn" ).instance()
		b.position = position - Vector2( 0, 10 )
		b.scale.x = -hit_dir
		get_parent().add_child( b )

func burn( dir ):
	state_nxt = STATES.BURN
	is_hit = true


func is_obstacle():
	if not $rotate/down.is_colliding(): return true
	if $rotate/front.is_colliding():
		if not $rotate/front.get_collider().is_in_group( "zombie" ): return true
	
	#if $rotate/front.is_colliding() or not $rotate/down.is_colliding():
	#	return true
	return false

var last_sight = 0.5
func player_in_sight( full = false, viewdir = 1 ):
#	print( last_sight )
#	last_sight -= get_physics_process_delta_time()
#	if last_sight > 0: return false
	if game.player == null: return false
	var start_pos = $target.global_position# - Vector2( 0, 11 )
	var end_pos = game.player.target.global_position
	if full:
		var dist = end_pos - start_pos
		var minreach = 160
		if short_ray: minreach /= 2
		if dist.length() > minreach and not long_ray: return false
		if sign( dist.x ) != viewdir * rotate.scale.x:
			return false
		if abs( start_pos.y - end_pos.y ) > 16:
			return false
	var space_state = get_world_2d().direct_space_state
	
	var result = space_state.intersect_ray( start_pos, end_pos, \
		[ self, $rotate/attack ], 1 + pow( 2, 11 ) )
	if not result.empty():
		if result.collider.get_parent() == game.player:
#			last_sight = 0.5
			return true
	return false




func play_roar():
	game.play_sfx( game.SFX_ZOMBIE_ROAR )







