extends ParallaxBackground

#export var start = 0

func _ready():
	call_deferred( "set_initial_sky" )

func set_initial_sky():
	if game.gamestate.sky == "day":
		print( "setting initial day" )
		$anim.play_backwards( "day_night" )
		$anim.seek(0.1)
	else:
		print( "setting initial night" )
		$anim.play( "day_night" )
		$anim.seek(0.9)



func night():
	print( "Changing from day to night" )
	$anim.play( "day_night" )
	game.gamestate.sky = "night"

func day():
	$anim.play_backwards( "day_night" )
	print( "Changing from night to day" )
	game.gamestate.sky = "day"

func fire():
	$fireanim.play( "cycle" )