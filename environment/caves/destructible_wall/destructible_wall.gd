extends StaticBody2D

signal wall_destroyed

var cur_shot = 0


func shot():
	#print( "cur shot: ", cur_shot )
	match cur_shot:
		0:
			$AnimationPlayer.play( "shot 1" )
			#game.play_sfx( game.SFX_WALL, false )
			cur_shot = 1
		1:
			$AnimationPlayer.play( "shot 2" )
			#game.play_sfx( game.SFX_WALL, false )
			cur_shot = 2
		2:
			$AnimationPlayer.play( "shot 3" )
			#game.play_sfx( game.SFX_WALL, false )
			cur_shot = 3
			emit_signal( "wall_destroyed" )

func clear_collisions():
	set_collision_layer_bit( 0, false )
	set_collision_mask_bit( 0, false )
	set_collision_mask_bit( 1, false )
	set_collision_mask_bit( 2, false )













	