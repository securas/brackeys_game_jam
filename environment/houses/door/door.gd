extends Area2D


func interact():
	#print( "door ", name, " opening" )
	set_collision_layer_bit( 5, false )
	set_collision_mask_bit( 5, false )
	$anim.play( "open" )
	$polygon_anim.play( "cycle" )
	$StaticBody2D.queue_free()
	game.play_sfx( game.SFX_DOOR )