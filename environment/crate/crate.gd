extends KinematicBody2D


var vel = Vector2()
func _physics_process(delta):
	vel.y += 1000 * delta
	vel.x *= 0.9
	vel = move_and_slide( vel )
	