extends Area2D

signal box

func interact():
	if game.add_event( "first box of food" ):
		game.message( "finally, some food", false )
	# todo: start event
	game.gamestate.energy = 1
	game.gamestate.stamina = 1
	set_collision_layer_bit( 5, false )
	set_collision_mask_bit( 5, false )
	$anim.play( "open" )
	game.play_sfx( game.SFX_FOOD )
	emit_signal( "box" )
	print( name, " opened" )