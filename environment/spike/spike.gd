extends Area2D


func _on_spike_area_entered(area):
	set_collision_layer_bit( 5, false )
	set_collision_mask_bit( 5, false )
	
	if area.get_parent() == game.player:
		game.player.global_position = global_position
		game.player.hit_fast( game.player.rotate.scale.x, false )
		$AnimationPlayer.play( "blood" )
		game.camera_shake( 0.5, 20, 8 )
		game.play_sfx( game.SFX_STEP, false )
		game.play_sfx( game.SFX_STEP, false )
		game.play_sfx( game.SFX_STEP, false )
