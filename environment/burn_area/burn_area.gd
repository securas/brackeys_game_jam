extends Area2D





func _on_burn_area_area_entered(area):
	var body = area.get_parent()
	if body.is_in_group( "zombie" ):
		body.burn( body.rotate.scale.x )
	pass # replace with function body
