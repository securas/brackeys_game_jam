extends Node

var player setget _set_player, _get_player
var camera setget _set_camera, _get_camera
var text_message setget _set_text_message, _get_text_message
var gamestate setget _set_gamestate
var main = null
var music setget _set_music, _get_music
var sfx = null

#===========================
func _set_player( v ):
	player = weakref( v )
func _get_player():
	if player == null: return null
	return player.get_ref()

#===========================
func _set_camera( v ):
	camera = weakref( v )
func _get_camera():
	if camera == null: return null
	return camera.get_ref()

#===========================
func _set_music( v ):
	music = weakref( v )
func _get_music():
	if music == null: return null
	return music.get_ref()

#===========================
func _set_text_message( v ):
	text_message = weakref( v )
func _get_text_message():
	if text_message == null: return null
	return text_message.get_ref()








func _ready():
	#Input.set_mouse_mode( Input.MOUSE_MODE_HIDDEN )
	self.pause_mode = PAUSE_MODE_PROCESS
	set_initial_gamestate()




#func _process(delta):
#	if Input.is_key_pressed( KEY_ESCAPE ):
#		get_tree().quit()



func set_initial_gamestate():
	gamestate = {
		"energy": 1, \
		"stamina": 1, \
		"start_position": null, \
		"events": [], \
		"sky": "day", \
		"new_zombies": { \
			"level_1": [] } \
		 }

func _set_gamestate( v ):
	gamestate = v
	if main != null:
		main.update_hud()

func is_event( evtname ):
	if gamestate[ "events" ].find( evtname ) == -1:
		return false
	return true
func add_event( evtname ):
	if gamestate[ "events" ].find( evtname ) == -1:
		gamestate[ "events" ].append( evtname )
		return true
	return false

func restart_level():
	gamestate.energy = 1
	gamestate.stamina = 1


func message( msg, player_input, wait_time = 2, speed = 20, pause_game = false ):
	if text_message != null and text_message.get_ref() != null:
		text_message.get_ref().player_input = player_input
		text_message.get_ref().base_text = msg
		text_message.get_ref().speed = speed
		text_message.get_ref().wait_time = wait_time
		text_message.get_ref().pause_game = pause_game
		text_message.get_ref().start_text()
		return text_message


var songs = [ \
	preload( "res://music/Intro.ogg" ), \
	preload( "res://music/Survivor.ogg" ), \
	preload( "res://music/Caves.ogg" ) ]
var curstreamno = -1

func play_music( streno, repeatplay = false ):
	if music == null or music.get_ref() == null: return
	if streno == curstreamno and not repeatplay:
		return
	curstreamno = streno
	var m = music.get_ref()
	m.stream = songs[streno]
	m.play()

enum SFX { SFX_STEP, SFX_SHOT, SFX_DOOR, SFX_WALL, SFX_FOOD, SFX_ZOMBIE_ROAR, SFX_PLAYER_HIT }
var sfxs = [ \
	preload( "res://sfx/steps.wav" ), \
	preload( "res://sfx/gunshot.wav" ), \
	preload( "res://sfx/door.wav" ), \
	preload( "res://sfx/cave_wall.wav" ), \
	preload( "res://sfx/food.wav" ), \
	preload( "res://sfx/zombie_roar.wav" ), \
	preload( "res://sfx/player_hit.wav" ) \
	]
func play_sfx( no, randompitch = true ):
	if sfx == null: return
	if randompitch:
		sfx.set_pitch( rand_range( -0.3, 0.3 ) + 1 )
	else:
		sfx.set_pitch( 1 )
	sfx.mplay( sfxs[no] )
	pass


func camera_shake(duration, frequency, amplitude):
	if camera == null or camera.get_ref() == null:
		#print( "no camera" )
		return
	camera.get_ref().shake(duration, frequency, amplitude)




