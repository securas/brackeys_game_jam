extends "res://screens/generic_screen.gd"


var timer = 3
func _physics_process(delta):
	timer -= delta
	if timer <= 0:
		if Input.is_action_just_pressed( "btn_shoot" ) or \
			Input.is_action_just_pressed( "btn_interact" ):
			emit_signal( "finished_level", "res://screens/start_menu.tscn" )
			set_physics_process( false )


