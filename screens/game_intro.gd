extends "res://screens/generic_screen.gd"

func _ready():
	game.gamestate.sky = "day"

var timer = 3
func _physics_process(delta):
	timer -= delta
	if timer <= 0:
		if Input.is_action_just_pressed( "btn_shoot" ) or \
			Input.is_action_just_pressed( "btn_interact" ):
			emit_signal( "finished_level", "res://levels/level_1.tscn" )
			set_physics_process( false )


func start_game():
	emit_signal( "finished_level", "res://levels/level_1.tscn" )