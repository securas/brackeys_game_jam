extends Node2D


signal restart_level
signal finished_level
signal game_over

func _input( event ):
	if event.is_action_pressed( "btn_quit" ):
		emit_signal( "game_over" )