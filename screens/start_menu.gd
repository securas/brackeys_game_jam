extends "res://screens/generic_screen.gd"

func _ready():
	game.play_music( 0 )

func _input(event):
	if event.is_action_pressed( "btn_shoot" ) or \
		event.is_action_pressed( "btn_jump" ) or \
		event.is_action_pressed( "btn_interact" ):
		emit_signal( "finished_level", "res://screens/game_intro.tscn" )
	
