extends "./level.gd"

var is_in_final_zone = false
var final_scene = false

func _ready():
	game.play_music( 1 )
	game.gamestate.sky = "day"
	starting_energy = 1
	$player.connect( "player_dead", self, "_on_player_dead" )
	call_deferred( "_set_zombies" )
	game.player.energy_decrease_multiplier = 2

func _on_player_dead():
	if not is_in_final_zone:
		emit_signal( "restart_level" )
	else:
		var ps = preload( "res://player/finalscene/player_final_scene.tscn" ).instance()
		add_child( ps )
		ps.connect( "end_scene", self, "_finished_cutscene" )
		$cutscene/polygons/cutanim.play( "cycle" )
		pass



func _on_finish_area_finish_area():
	print( "finished level 3" )
	#emit_signal( "finished_level", "res://levels/level_2.tscn" )


func _on_player_low_energy():
		pass


func _on_event_area_3_final_zone():
	is_in_final_zone = true

func _finished_cutscene():
	game.gamestate.new_zombies.level_1 = []
	emit_signal( "finished_level", "res://screens/end_game.tscn" )
	pass
