extends "res://screens/generic_screen.gd"

export var starting_energy = 0.7

func _set_zombies( ):
	#print( "Setting new zombies" )
	game.gamestate.energy = self.starting_energy
	# new zombies
	for idx in range( game.gamestate.new_zombies.level_1.size() - 1, -1, -1 ):
		var p = game.gamestate.new_zombies.level_1[idx]
		var space_state = get_world_2d().direct_space_state
		var result = space_state.intersect_point( p, 32, [], 1 + 4 )
		if result.empty() and ( game.player.global_position - p ).length() > 80:
			var z = preload( "res://zombie/zombie.tscn" ).instance()
			z.position = p + Vector2( 0, -4 )
			add_child( z )
			break


