extends Node2D

func end_it():
	$polygons.queue_free()
	$walls.queue_free()
	$event_area_2.queue_free()
	$torches.queue_free()
	queue_free()
	pass