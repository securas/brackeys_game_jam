extends Area2D

export var is_active = true
var set = false




func _on_restart_position_area_entered(area):
	if area.get_parent() != game.player:
		return
	
	if not is_active: return
	if set: return
	set = true
	print( name, " setting restart position ", $position.global_position, " triggered by ", area.name, " ", area.get_parent().name )
	
	game.gamestate.start_position = $position.global_position
	game.gamestate.start_energy = min( 1, game.gamestate.energy * 1.5 )




func _on_box():
	print( name, " activated restart area" )
	is_active = true
