extends "./level.gd"



func _ready():
	game.play_music( 2 )
	$player.connect( "player_dead", self, "_on_player_dead" )
	call_deferred( "_set_zombies" )

func _on_player_dead():
	$environment.end_it()
	emit_signal( "restart_level" )


func _on_finish_area_finish_area():
	game.gamestate.new_zombies.level_1 = []
	emit_signal( "finished_level", "res://levels/level_3.tscn" )