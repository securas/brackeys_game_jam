extends Node2D



func _on_player_final_scene_final_message():
	game.message( "I could not save her...", false )
	if game.text_message != null: yield( game.text_message, "text_finished" )
	game.message( "Now I'm the last one", false, 4 )
	if game.text_message != null: yield( game.text_message, "text_finished" )


