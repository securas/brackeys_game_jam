extends KinematicBody2D

signal final_message
signal end_scene

var state = 0

var vel = Vector2()
var anim_cur = ""
var anim_nxt = "run"

func _ready():
	position = game.player.position + Vector2( 160, -10 )
	$rotate.scale.x = -1

var timer = 0
func _physics_process(delta):
	vel.y += 1000 * delta
	vel = move_and_slide( vel )
	timer -= delta
	match state:
		0:
			# move towards player
			vel.x = -game.player.RUN_VEL
			anim_nxt = "run"
			if ( position - game.player.position ).length() < 50:
				state = 1
		1:
			# walk towards player
			vel.x = -game.player.WALK_VEL
			anim_nxt = "walk"
			if ( position - game.player.position ).length() < 10:
				state = 2
		2:
			vel.x = 0
			anim_nxt = "idle"
			timer = 2
			state = 3
		3:
			if timer <= 0:
				emit_signal( "final_message" )
				state = 4
				timer = 4
		4:
			if timer <= 0:
				state = 5
		5:
			if Input.is_action_just_pressed( "btn_shoot" ) or \
				Input.is_action_just_pressed( "btn_interact" ):
					emit_signal( "end_scene" )
					state = 6
			pass
	
	if anim_nxt != anim_cur:
		anim_cur = anim_nxt
		$anim.play( anim_cur )