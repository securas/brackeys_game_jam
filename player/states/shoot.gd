extends "./state.gd"

enum STATES { SHOOTING, WAITING, FINISH }
var shoot_timer = 0
var state = 0

func initialize( obj ):
	obj.anim_nxt = "shoot"
	state = STATES.SHOOTING
	shoot_timer = 0.2

func run_state( obj, delta ):
	if state == STATES.WAITING:
		obj.anim_nxt = "idle_shoot"
		shoot_timer -= delta
		if shoot_timer <= 0:
			state = STATES.FINISH
	elif state == FINISH:
		obj.idle_animation = "idle_shoot"
		obj.state_nxt = obj.STATES.IDLE



func finished_shoot():
	state = STATES.WAITING
	pass


