extends "./state.gd"

var tired_timer

func initialize( obj ):
	obj.anim_nxt = "tired"
	tired_timer = obj.RECOVERY_TIME
	
	if game.add_event( "first time tired" ):
		game.message( "Ufff... I need a break", false, 2, 20, false )
		pass

func run_state( obj, delta ):
	# gravity
	obj.vel.y = min( obj.vel.y + obj.GRAVITY * delta, obj.TER_VEL )
	
	# dampen horizontal motion
	obj.vel.x = 0#lerp( obj.vel.x, 0, delta * obj.DECEL )
	
	# move
	obj.vel = obj.move_and_slide( obj.vel, Vector2( 0, -1 ) )
	
	game.gamestate.stamina = min( 1, game.gamestate.stamina + delta / obj.RECOVERY_TIME )
	tired_timer -= delta
	if tired_timer <= 0:
		obj.state_nxt = obj.STATES.IDLE

func terminate( obj ):
	game.gamestate.stamina = 1