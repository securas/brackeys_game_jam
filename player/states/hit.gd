extends "./state.gd"

var signal_timer

func initialize( obj ):
	if obj.hit_normal:
		obj.anim_nxt = "dead"
		signal_timer = 3
	else:
		obj.anim_nxt = "dead_fast"
		signal_timer = 2

func run_state( obj, delta ):
	if signal_timer >= 0:
		signal_timer -= delta
		if signal_timer <= 0:
			obj.emit_signal( "player_dead" )
	else:
		return
	# gravity
	obj.vel.y = min( obj.vel.y + obj.GRAVITY * delta, obj.TER_VEL )
	
	# dampen horizontal motion
	obj.vel.x = 0#lerp( obj.vel.x, 0, delta * obj.DECEL )
	
	# move
	obj.vel = obj.move_and_slide( obj.vel, Vector2( 0, -1 ) )
	
	
		