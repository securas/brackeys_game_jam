extends "res://player/states/state.gd"
var margin = 0.2
var waiting
var no_interaction = false
var viewtimer = 0.5
func initialize( obj ):
	margin = 0.2
	waiting = false
	start_interaction(obj)

func run_state( obj, delta ):
	
	viewtimer -= delta
	if viewtimer <= 0:
		obj.state_nxt = obj.STATES.IDLE
#	if waiting:
#		margin -= delta
#		if margin > 0:
#			# check player input
#			if Input.is_action_pressed( "btn_left" ) or Input.is_action_pressed( "btn_right" ):
#				obj.state_nxt = obj.STATES.WALK
#		else:
#			waiting = false
#			start_interaction(obj)
#	else:
#		viewtimer -= delta
#		if viewtimer <= 0:
#			obj.state_nxt = obj.STATES.IDLE

func start_interaction(obj):
	# check for interactions
	viewtimer = 0.1
	obj.anim_nxt = "interact"
	var areas = obj.interbox.get_overlapping_areas()
	if not areas.empty():
		if areas[0].is_in_group( "box" ):
			obj.anim_nxt = "duck"
			viewtimer = 0.5
		#print( "Interacting with ", areas[0].name )
		if areas[0].has_method( "interact" ):
			areas[0].interact()

