extends "./state.gd"

var jump_timer = 0
#var anim_timer = 0
var max_hvel

func initialize( obj ):
	game.play_sfx( game.SFX_STEP )
	# jumping dust
	#obj.jumping_dust()
	# animation
	obj.anim_nxt = "jump"
	# set jump
	obj.is_jump = true
	#obj.anim_nxt = "jumping"
#	anim_timer = 0
	obj.vel.y = -obj.JUMP_VEL
	jump_timer = obj.JUMP_TIME
	max_hvel = max( abs( obj.vel.x ), obj.MAX_VEL_AIR )
	game.gamestate.stamina = max( 0, game.gamestate.stamina - obj.JUMP_STAMINA )
	

func run_state( obj, delta ):
	# gravity
	jump_timer -= delta
	if Input.is_action_pressed( "btn_jump" ) and jump_timer >= 0:
		obj.vel.y = min( obj.vel.y + obj.JUMP_GRAV * delta, obj.TER_VEL )
	else:
		obj.vel.y = min( obj.vel.y + obj.GRAVITY * delta, obj.TER_VEL )

	# move
	obj.vel = obj.move_and_slide( obj.vel, Vector2( 0, -1 ) )
	
	# check if on floor
	if obj.is_on_floor():
		if abs( obj.vel.x ) < 1:
			obj.state_nxt = obj.STATES.IDLE
		else:
			obj.state_nxt = obj.STATES.RUN
		obj.is_jump = false
		# effects
#		obj.landing_dust()
	elif obj.is_on_ceiling():
		obj.vel.y *= 0
		obj.state_nxt = obj.STATES.FALL
	elif obj.vel.y > 0:
		obj.state_nxt = obj.STATES.FALL
		#print( "MAX POS: ", obj.position.y )
	else:
		# check player input
		if Input.is_action_pressed( "btn_left" ):
			if obj.is_on_wall():
				#obj.state_nxt = obj.STATES.WALL_GRAB
				pass
			else:
				obj.vel.x = lerp( obj.vel.x, -max_hvel, delta * obj.ACCEL_AIR )
				obj.dir_nxt = -1
		elif Input.is_action_pressed( "btn_right" ):
			if obj.is_on_wall():
				#obj.state_nxt = obj.STATES.WALL_GRAB
				pass
			else:
				obj.vel.x = lerp( obj.vel.x, max_hvel, delta * obj.ACCEL_AIR )
				obj.dir_nxt = 1
		else:
			obj.vel.x = lerp( obj.vel.x, 0, delta * obj.DECEL_AIR )












