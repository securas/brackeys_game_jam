extends "./state.gd"

#var anim_timer = 0

func initialize( obj ):
	obj.anim_nxt = obj.idle_animation #"idle"

func run_state( obj, delta ):
	# gravity
	obj.vel.y = min( obj.vel.y + obj.GRAVITY * delta, obj.TER_VEL )
	
	# dampen horizontal motion
	obj.vel.x = 0#lerp( obj.vel.x, 0, delta * obj.DECEL )
	#if obj.down.is_colliding():
	#	obj.vel.y *= 0
	
	# move
	obj.vel = obj.move_and_slide( obj.vel, Vector2( 0, -1 ) )

	
	# check if on floor
	if not obj.is_on_floor() and not obj.down.is_colliding():
		obj.state_nxt = obj.STATES.FALL
	else:
		# check player input
		if Input.is_action_pressed( "btn_left" ) or Input.is_action_pressed( "btn_right" ):
			#print( "idle vel: ", obj.vel )
			obj.state_nxt = obj.STATES.RUN#obj.STATES.WALK
		if Input.is_action_just_pressed( "btn_jump" ):
			obj.state_nxt = obj.STATES.JUMP
#		if Input.is_action_just_pressed( "btn_up" ):
#			if is_on_stairs( obj ):
#				# change to stairs state
#				pass
#			else:
#				# change camera target
#				pass
		if Input.is_action_pressed( "btn_down" ):
			obj.state_nxt = obj.STATES.DUCK
		if Input.is_action_just_pressed( "btn_interact" ):
			obj.state_nxt = obj.STATES.INTERACT

func terminate( obj ):
	obj.idle_animation = "idle"