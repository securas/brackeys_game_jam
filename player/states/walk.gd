extends "./state.gd"

var anim_timer
var collision_normal

func initialize( obj ):
	anim_timer = 0
	collision_normal = Vector2( 0, -1 )
	obj.anim_nxt = "walk"

func run_state( obj, delta ):
	# gravity based on the latest collision normal: todo: adjust this to NEXT collision normal
	
	collision_normal = obj.down.get_collision_normal()
	var gravity = -collision_normal * Vector2( 1, obj.GRAVITY * delta )
	if acos( collision_normal.dot( Vector2( 0, -1 ) ) ) < ( PI/4 + 0.05 ):
		obj.vel.y -= 5
	obj.vel += gravity
	
	# move
	#obj.vel = obj.move_and_slide( obj.vel, Vector2( 0, -1 ) )
	#var expected_slide_velocity = obj.vel.slide( collision_normal )
	obj.vel = obj.move_and_slide( obj.vel, Vector2( 0, -1 ) )
	#if obj.vel.normalized().dot( expected_slide_velocity.normalized() ) < 0.9:
	#	obj.vel = obj.move_and_slide( expected_slide_velocity / 1.4, Vector2( 0, -1 ) )
	
	# push crate
	if obj.push.is_colliding():
		var c = obj.push.get_collider()
		c.move_and_slide( Vector2( obj.rotate.scale.x * 500 * delta, 0 ) )
		obj.anim_nxt = "push"
		# stamina
		game.gamestate.stamina -= obj.STAMINA_DECREASE_RATE * delta
		if game.gamestate.stamina <= 0:
			game.gamestate.stamina = 0
			obj.state_nxt = obj.STATES.TIRED
	else:
		game.gamestate.stamina = min( 1, game.gamestate.stamina + obj.STAMINA_INCREASE_RATE * delta )
		obj.anim_nxt = "walk"
	
	# check if on floor
	if obj.is_on_floor() or obj.down.is_colliding():
		# check player input
		var is_moving = false
		if Input.is_action_pressed( "btn_left" ):
			obj.vel.x = -obj.WALK_VEL
			obj.dir_nxt = -1
			is_moving = true
		elif Input.is_action_pressed( "btn_right" ):
			obj.vel.x = obj.WALK_VEL
			is_moving = true
			obj.dir_nxt = 1
		if not is_moving:
			obj.state_nxt = obj.STATES.IDLE
		elif Input.is_action_pressed( "btn_interact" ):
				obj.state_nxt = obj.STATES.RUN
		if Input.is_action_just_pressed( "btn_jump" ):
			obj.state_nxt = obj.STATES.JUMP
	else:
		obj.state_nxt = obj.STATES.FALL


