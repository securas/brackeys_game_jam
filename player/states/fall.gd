extends "./state.gd"

var fall_timer = 0
var anim_timer = 0

#var from_dash

func initialize( obj ):
	fall_timer = 0
	anim_timer = 0.1
	obj.anim_nxt = "fall"
	if obj.is_jump:
		obj.anim_nxt = "fall_begin"
	
#	from_dash = false
#	if obj.vel.y > obj.TER_VEL:
#		from_dash = true

func run_state( obj, delta ):
	# animation
	anim_timer -= delta
	if anim_timer <= 0:
		obj.anim_nxt = "fall"
	# fall timer
	fall_timer += delta
	
	# gravity
	obj.vel.y = min( obj.vel.y + obj.GRAVITY * delta, obj.TER_VEL )
	
	# move
	obj.vel = obj.move_and_slide( obj.vel, Vector2( 0, -1 ) )
	
	# check if on floor
	if obj.is_on_floor():
		# change to landing state or idle state, depending on the fall time
		game.play_sfx( game.SFX_STEP )
		if fall_timer > obj.LARGE_FALL:
			game.camera_shake( 0.5, 20, 8 )
			obj.hit_fast( obj.scale.x, false )
			#obj.state_nxt = obj.STATES.IDLE
			pass
		else:
			obj.state_nxt = obj.STATES.IDLE
			
			
		# effects
		#obj.landing_dust()
	else:
		# check player input
		if Input.is_action_pressed( "btn_left" ):
			if obj.is_on_wall():
				#obj.state_nxt = obj.STATES.WALL_GRAB
				pass
			else:
				obj.vel.x = lerp( obj.vel.x, -obj.MAX_VEL_AIR, delta * obj.ACCEL_AIR )
				obj.dir_nxt = -1
		elif Input.is_action_pressed( "btn_right" ):
			if obj.is_on_wall():
				#obj.state_nxt = obj.STATES.WALL_GRAB
				pass
			else:
				obj.vel.x = lerp( obj.vel.x, obj.MAX_VEL_AIR, delta * obj.ACCEL_AIR )
				obj.dir_nxt = 1
		else:
			obj.vel.x = lerp( obj.vel.x, 0, delta * obj.DECEL_AIR )
			if abs( obj.vel.x ) < 1:
				obj.vel.x = 0
		
		#print( obj.is_jump, " ", fall_timer, " ", obj.JUMP_MARGIN )
		if not obj.is_jump and Input.is_action_just_pressed( "btn_jump" ) and fall_timer < obj.JUMP_MARGIN:
			obj.state_nxt = obj.STATES.JUMP

func terminate( obj ):
	obj.is_jump = false
#	obj.is_dash = false
