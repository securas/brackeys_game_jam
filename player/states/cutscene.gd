extends "./state.gd"

#var anim_timer = 0

func initialize( obj ):
	obj.anim_nxt = obj.idle_animation

func run_state( obj, delta ):
	# gravity
	obj.vel.y = min( obj.vel.y + obj.GRAVITY * delta, obj.TER_VEL )
	
	# dampen horizontal motion
	obj.vel.x = 0
	
	# move
	obj.vel = obj.move_and_slide( obj.vel, Vector2( 0, -1 ) )


func terminate( obj ):
	obj.idle_animation = "idle"