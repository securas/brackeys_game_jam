extends KinematicBody2D

signal player_dead
signal low_energy

# constants
const WALK_VEL = 20
const RUN_VEL = 60
#const ACCEL = 10 # horizontal acceleration
#const DECEL = 100 # horizontal deceleration
const MAX_VEL_AIR = 40#30 # max velocity on air
const ACCEL_AIR = 5#5 # horizontal acceleration on air
const DECEL_AIR = 5#5 # horizontal decceleration on air
const GRAVITY = 1000 
const TER_VEL = 150 # terminal velocity
const JUMP_VEL = 200 # jump velocity
const JUMP_GRAV = 500 # gravity during initial part of the jump
const JUMP_TIME = 0.4 # time during jump to restore normal gravity
const JUMP_MARGIN = 0.35 # margin when falling when it is still possible to jump
const LARGE_FALL = 1 # if a fall lasts this long, there will be some recovery time
const LAND_TIME = 0.5 # time to wait if the fall is very large
const RECOVERY_TIME = 3
const STAMINA_DECREASE_RATE = 0.08
const STAMINA_INCREASE_RATE = 0.25
const JUMP_STAMINA = 0.12
const ENERGY_DECREASE_RATE = 0.013

var energy_decrease_multiplier = 1

#const INITIAL_ENERGY = 5

# states machine
onready var STATES = { \
	"IDLE": $states/idle, \
	"FALL": $states/fall, \
#	"LAND": $states/land, \
#	"WALK": $states/walk, \
	"RUN": $states/run, \
	"JUMP": $states/jump, \
	"SHOOT": $states/shoot, \
	"HIT": $states/hit, \
	"DUCK": $states/duck, \
	"TIRED": $states/tired, \
	"INTERACT": $states/interact, \
	"CUTSCENE": $states/cutscene, \
	"FINAL_CUTSCENE": $states/final_cutscene
	}
var state_cur = null
onready var state_nxt = STATES.IDLE setget _set_state_nxt

# special states
var is_jump = false
var is_fire = false
#var is_dash = false


var is_cutscene = false

# motion
var vel = Vector2()

# direction
var dir_nxt = 1
onready var rotate = $rotate
onready var down = $down
onready var target = $target
onready var interbox = $interactbox
onready var push = $rotate/push

# animation
var anim_cur =""
var anim_nxt = "idle"
var idle_animation = "idle"

# damage
var is_hit = false
var is_dead = false
var hit_dir = 0
#var energy = INITIAL_ENERGY

func _set_state_nxt( v ):
	# before setting the next state, check if hit
	# prevents other states from changing the next state
	if not is_hit:
		state_nxt = v


func _ready():
	game.restart_level()
	game.player = self
	call_deferred( "_start_conditions" )

func _start_conditions():
	if game.gamestate.start_position != null:
		global_position = game.gamestate.start_position
		game.gamestate.energy = game.gamestate.start_energy

func _physics_process( delta ):
	# update state
	if state_nxt != state_cur:
		if state_cur != null:
			#print( "changing from state ", state_cur.name, " to ", state_nxt.name )
			state_cur.terminate( self )
		state_cur = state_nxt
		state_cur.initialize( self )
	
	# run state
	state_cur.run_state( self, delta )
	
	# update direction
	if dir_nxt != rotate.scale.x:
		rotate.scale.x = dir_nxt
	
	# update animation
	if anim_nxt != anim_cur:
		anim_cur = anim_nxt
		$anim.play( anim_cur )
	
	# shoot
	if Input.is_action_just_pressed( "btn_shoot" ) and \
			is_on_floor() and \
			not is_hit and not is_dead and state_cur != STATES.TIRED and \
			not is_cutscene and \
			not $rotate/arm.is_colliding():
		state_nxt = STATES.SHOOT
	
	# stamina
#		state_cur != STATES.WALK and \
	if state_cur != STATES.TIRED and \
		state_cur != STATES.RUN and \
		state_cur != STATES.FALL and \
		state_cur != STATES.JUMP:
		game.gamestate.stamina = min( 1, game.gamestate.stamina + STAMINA_INCREASE_RATE * delta )
	
	# energy
	if not is_cutscene:
		game.gamestate.energy = max( 0, game.gamestate.energy - delta * ENERGY_DECREASE_RATE * energy_decrease_multiplier )
	if game.gamestate.energy < 0.5 and game.add_event( "first energy warning" ):
		game.message( "I must find some food soon", false )
	if game.gamestate.energy < 0.1: emit_signal( "low_energy" )
	if game.gamestate.energy <= 0 and not is_hit:
		hit( rotate.scale.x, false )
	
	



var hit_normal = true
func hit( dir = 1, blood_anim = true ):
	if is_hit:
		if blood_anim:
			var b = preload( "res://player/fx/blood.tscn" ).instance()
			b.position = position
			b.scale.x = rotate.scale.x
			get_parent().add_child( b )
		return
	hit_normal = true
	state_nxt = STATES.HIT
	is_hit = true
	game.play_sfx( game.SFX_PLAYER_HIT )
	if blood_anim:
		var b = preload( "res://player/fx/blood.tscn" ).instance()
		b.position = position
		b.scale.x = rotate.scale.x
		get_parent().add_child( b )
	if state_cur != STATES.HIT and blood_anim:
		game.gamestate.new_zombies.level_1.append( global_position )

func hit_fast( dir = 1, blood_anim = true ):
	if is_hit:
		if blood_anim:
			var b = preload( "res://player/fx/blood.tscn" ).instance()
			b.position = position
			b.scale.x = rotate.scale.x
			get_parent().add_child( b )
		return
	hit_normal = false
	state_nxt = STATES.HIT
	is_hit = true
	game.play_sfx( game.SFX_PLAYER_HIT )
	if blood_anim:
		var b = preload( "res://player/fx/blood.tscn" ).instance()
		b.position = position
		b.scale.x = rotate.scale.x
		get_parent().add_child( b )
	if state_cur != STATES.HIT and blood_anim:
		game.gamestate.new_zombies.level_1.append( global_position )


func cutscene( setcs ): 
	if setcs:
		is_cutscene = true
		state_nxt = STATES.CUTSCENE
	else:
		is_cutscene = false
		state_nxt = STATES.IDLE




#========================================
# functions to examine terrain
#========================================
func compute_shoot():
	game.play_sfx( game.SFX_SHOT, false )
	# check arm
	if $rotate/arm.is_colliding(): return
	var space_state = get_world_2d().direct_space_state
	var start_pos = $rotate/shoot_pos.global_position
	var result = space_state.intersect_ray( start_pos, start_pos + Vector2( rotate.scale.x * 320, 0 ), \
		[ self ], 1 + pow( 2, 12 ) )
	if not result.empty():
		# type of collider
		#print( result )
		#print( result.collider.name, " ", result.collider.is_in_group( "destructible" ) )
		if result.collider.is_in_group( "zombie" ):
			result.collider.get_parent().hit( rotate.scale.x )
			
		elif result.collider.is_in_group( "destructible" ):
			result.collider.shot()
			var s = preload( "./fx/hit_wall.tscn" ).instance()
			s.global_position = result.position.round()
			s.rotation = ( -result.normal ).angle()#angle_to( Vector2( -1, 0 ) )
			get_parent().add_child( s )
			game.play_sfx( game.SFX_WALL, false )
		else:
			var s = preload( "./fx/hit_wall.tscn" ).instance()
			s.global_position = result.position.round()
			s.rotation = ( -result.normal ).angle()#angle_to( Vector2( -1, 0 ) )
			get_parent().add_child( s )
			game.play_sfx( game.SFX_WALL, false )
		game.camera_shake( 0.25, 60, 4 )
	else:
		game.camera_shake( 0.1, 60, 2 )
	pass


#========================================
# functions to create fx
#========================================
func shoot():
	var s = preload( "./fx/shoot.tscn" ).instance()
	s.global_position = $rotate/shoot_pos.global_position
	s.scale.x = rotate.scale.x
	get_parent().add_child( s )

#========================================
# functions to create sound effects
#========================================
func step_sfx():
	game.play_sfx( game.SFX_STEP )














