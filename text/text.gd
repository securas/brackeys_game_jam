extends Node2D

signal text_finished

var speed = 20
var wait_time = 2
export( String ) var base_text = ""
var pause_game = true

var player_input = false
var curpos = 0
var finished_text = false
var finished_waiting = false


func _ready():
	self.modulate = Color( 0, 0, 0, 0 )
	game.text_message = self
	



func wait_to_finish():
	if wait_time == 0:
		_on_wait_timer_timeout()
	else:
		$wait_timer.wait_time = wait_time
		$wait_timer.start()


func _input(event):
	if not player_input: return
	if Input.is_action_just_pressed( "btn_interact" ) or \
		Input.is_action_just_pressed( "btn_jump" ) or \
		Input.is_action_just_pressed( "btn_shoot" ):
			if not finished_text:
				$text_label.text = base_text
				finished_text = true
				wait_to_finish()
			else:
				_on_wait_timer_timeout()




func _on_text_timer_timeout():
	if finished_text: return
	curpos += 1
	if curpos <= base_text.length():
		$text_label.text = base_text.substr( 0, curpos )
	else:
		$text_timer.stop()
		finished_text = true
		wait_to_finish()


func _on_wait_timer_timeout():
	if finished_waiting: return
	finished_waiting = true
	end_text()


func start_text():
	# fade in
	$text_label.text = ""
	finished_text = false
	finished_waiting = false
	curpos = 0
	$anim.play( "fade_in" )

func finished_starting():
	if pause_game: get_tree().paused = true
	# start text
	if speed == 0:
		$text_label.text = base_text
		finished_text = true
		wait_to_finish()
	else:
		$text_timer.wait_time = 1.0 / speed
		curpos = 0
		$text_timer.start()
		$text_label.text = base_text.substr( 0, curpos )
	set_process_input( true )

func end_text():
	$anim.play( "fade_out" )
	if pause_game: get_tree().paused = false
	base_text = ""
	$text_label.text = ""
	curpos = 0
